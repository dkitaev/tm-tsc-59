package ru.tsc.kitaev.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kitaev.tm.api.repository.model.ISessionRepository;
import ru.tsc.kitaev.tm.api.repository.model.IUserRepository;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.model.ISessionService;
import ru.tsc.kitaev.tm.exception.user.AccessDeniedException;
import ru.tsc.kitaev.tm.exception.user.UserIsLockedException;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.util.HashUtil;

@Service
@AllArgsConstructor
public final class SessionService extends AbstractService implements ISessionService {

    @NotNull
    @Autowired
    public ISessionRepository sessionRepository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @Transactional
    public Session open(@Nullable final String login, @Nullable final String password) {
        @Nullable final User user = checkDataAccess(login, password);
        if (user == null) throw new AccessDeniedException();
        final Session session = new Session();
        @Nullable final User user2 = userRepository.findByLogin(login);
        session.setUser(user2);
        session.setTimestamp(System.currentTimeMillis());
        sign(session);
        sessionRepository.add(session);
        return session;
    }

    @Nullable
    @Override
    public User checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final User user = userRepository.findByLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(secret, iteration, password);
        if (user.getLocked()) throw new UserIsLockedException();
        if (user.getPasswordHash().equals(hash)) {
            return user;
        } else {
            return null;
        }
    }

    @NotNull
    @Override
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final String secret = propertyService.getSessionSecret();
        @NotNull final Integer iteration = propertyService.getSessionIteration();
        @Nullable final String signature = HashUtil.sign(secret, iteration, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @Transactional
    public void close(@Nullable Session session) {
        if (session == null) return;
        sessionRepository.removeById(session.getId());
    }

}
