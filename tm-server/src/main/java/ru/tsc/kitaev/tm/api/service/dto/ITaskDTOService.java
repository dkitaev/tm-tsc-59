package ru.tsc.kitaev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskDTOService {

    void clear();

    void clear(@Nullable final String userId);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAll(@Nullable final String userId);

    @NotNull
    List<TaskDTO> findAll(@Nullable final String userId, @Nullable final String sort);

    @Nullable
    TaskDTO findById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    TaskDTO findByIndex(@Nullable final String userId, @Nullable final Integer index);

    void removeById(@Nullable final String userId, @Nullable final String id);

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    boolean existsByIndex(@Nullable final String userId, final int index);

    @NotNull
    Integer getSize(@Nullable final String userId);

    void create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void addAll(@NotNull List<TaskDTO> tasks);

    @NotNull
    TaskDTO findByName(@Nullable String userId, @Nullable String name);

    void removeByName(@Nullable String userId, @Nullable String name);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description);

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @NotNull String description);

    void startById(@Nullable String userId, @Nullable String id);

    void startByIndex(@Nullable String userId, @Nullable Integer index);

    void startByName(@Nullable String userId, @Nullable String name);

    void finishById(@Nullable String userId, @Nullable String id);

    void finishByIndex(@Nullable String userId, @Nullable Integer index);

    void finishByName(@Nullable String userId, @Nullable String name);

    void changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    void changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
