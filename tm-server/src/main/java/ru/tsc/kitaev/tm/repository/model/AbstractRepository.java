package ru.tsc.kitaev.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.api.repository.model.IRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Getter
@Repository
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRepository implements IRepository {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

}
