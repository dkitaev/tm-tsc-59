package ru.tsc.kitaev.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.api.repository.dto.IDTORepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Getter
@Repository
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTORepository implements IDTORepository {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

}
