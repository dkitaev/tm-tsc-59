package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.kitaev.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kitaev.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @Override
    @NotNull
    @WebMethod
    public List<TaskDTO> findTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AbstractException {
        sessionService.validate(session);
        return projectTaskService.findTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void bindTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AbstractException {
        sessionService.validate(session);
        projectTaskService.bindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    public void unbindTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AbstractException {
        sessionService.validate(session);
        projectTaskService.unbindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    public void removeById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AbstractException {
        sessionService.validate(session);
        projectTaskService.removeById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void removeByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        sessionService.validate(session);
        projectTaskService.removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        sessionService.validate(session);
        projectTaskService.removeByName(session.getUserId(), name);
    }

}
