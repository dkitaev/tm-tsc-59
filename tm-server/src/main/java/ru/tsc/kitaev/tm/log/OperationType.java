package ru.tsc.kitaev.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
