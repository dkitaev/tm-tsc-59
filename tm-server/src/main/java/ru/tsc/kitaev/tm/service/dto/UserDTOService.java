package ru.tsc.kitaev.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kitaev.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.dto.IUserDTOService;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.exception.user.EmailExistsException;
import ru.tsc.kitaev.tm.exception.user.LoginExistsException;
import ru.tsc.kitaev.tm.exception.user.UserNotFoundException;
import ru.tsc.kitaev.tm.util.HashUtil;

import java.util.List;

@Service
@AllArgsConstructor
public final class UserDTOService extends AbstractDTOService implements IUserDTOService {

    @NotNull
    @Autowired
    public IUserDTORepository userRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @Transactional
    public void clear() {
        userRepository.clear();
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public UserDTO findById(@NotNull final String id) {
        return userRepository.findById(id);
    }

    @NotNull
    @Override
    public UserDTO findByIndex(@NotNull final Integer index) {
        if (index < 0) throw new EmptyIndexException();
        return userRepository.findByIndex(index);
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String id) {
        @Nullable final UserDTO user = userRepository.findById(id);
        if (user == null) throw new UserNotFoundException();
        userRepository.remove(user);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return userRepository.findById(id) != null;
    }

    @Override
    public boolean existsByIndex(final int index) {
        userRepository.findByIndex(index);
        return true;
    }

    @Override
    @Transactional
    public void addAll(@NotNull final List<UserDTO> users) {
        for (UserDTO user : users) {
            userRepository.add(user);
        }
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login) == null;
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email) == null;
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        userRepository.remove(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (!isLoginExists(login)) throw new LoginExistsException();
        @NotNull final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        setPassword(user, password);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (!isLoginExists(login)) throw new LoginExistsException();
        if (!isEmailExists(email)) throw new EmailExistsException();
        @NotNull final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        setPassword(user, password);
        user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (!isLoginExists(login)) throw new LoginExistsException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        setPassword(user, password);
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @Override
    @Transactional
    public void setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserDTO user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        setPassword(user, password);
        userRepository.update(user);
    }

    @Override
    public void setPassword(@Nullable final UserDTO user, @Nullable final String password) {
        if (user == null) throw new EntityNotFoundException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(secret, iteration, password);
        user.setPasswordHash(hash);
    }

    @Override
    @Transactional
    public void updateUser(
            @Nullable final String userId,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final UserDTO user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        userRepository.update(user);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        userRepository.update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        userRepository.update(user);
    }

}
