package ru.tsc.kitaev.tm.listener.project;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.endpoint.ProjectDTO;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.listener.AbstractListener;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    protected void showProject(@Nullable ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

}
